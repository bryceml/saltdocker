# Release Process

How do new images of dockerized Salt make it into DockerHub? Follow these steps
to make awesome happen!

## Prerequisites

Before images can be created, and pushed to DockerHub, the target version must
exist as a [PyPi `salt` release](https://pypi.org/project/salt/#history).

If it does, fork this repo and create a new branch in your fork.

## New Releases: MRs to `saltdocker` repo

There are two kinds of releases:

- Stable/Latest: All new major and minor version releases
- Release Candidate/RC: All pre-release versions of `salt`

### Update `README.md`

Both `rc` and stable releases require updating the **Supported tags and**
**respective `Dockerfile` links** section of the `README.md` file.

Example:

Do you want to update for a new major release, being `3003`? Here is what parts of
the `README.md` file shows before an update:

> - [`3002.5`, `3002.4`, `3002.3`, `3002.2`, `3002.1`, `3002`, `latest`](https://gitlab.com/saltstack/open/saltdocker/-/blob/master/cicd/docker/alpine.Dockerfile)
> - [`3001.6`, `3001.5`, `3001.4`, `3001.3`, `3001.2`, `3001.1`, `3001`](https://gitlab.com/saltstack/open/saltdocker/-/blob/master/cicd/docker/alpine.old.Dockerfile)

Here is what it would show after:

> - [`3003`, `latest`](https://gitlab.com/saltstack/open/saltdocker/-/blob/master/cicd/docker/alpine.Dockerfile)
> - [`3002.5`, `3002.4`, `3002.3`, `3002.2`, `3002.1`, `3002`](https://gitlab.com/saltstack/open/saltdocker/-/blob/master/cicd/docker/alpine.Dockerfile)
> - [`3001.6`, `3001.5`, `3001.4`, `3001.3`, `3001.2`, `3001.1`, `3001`](https://gitlab.com/saltstack/open/saltdocker/-/blob/master/cicd/docker/alpine.old.Dockerfile)

If it is an `rc` release, the update would instead look like:

> - [`3003rc1`, `rc`](https://gitlab.com/saltstack/open/saltdocker/-/blob/master/cicd/docker/alpine.Dockerfile)

**Key things to know:**

- The `rc` tag is tied to the latest `rc` release
- The `latest` tag is tied to the latest release/point release for the current major version
  - `3002`, `3002.1`, `3002.2`, etc. when `3002.x` is the current major release

### Update `.gitlab-ci.yml`

This will differ, depending on `rc` vs. `latest` releases.

- `rc` release: Change the `SALT_VERSION` in `salt-alpine-rc` to the new
  `rc` version
  - Add the previous `SALT_VERSION` to the list in `salt-alpine-post-aluminum-not-latest`

- `latest` release: Change the `SALT_VERSION` in `salt-alpine-latest` to the new
  `latest` version
  - Add the previous `SALT_VERSION` to the list in `salt-alpine-post-aluminum-not-latest`

> If the release is a patch to a non-latest major release, then it only needs to
> be added the `SALT_VERSION` list in the appropriate GitLab CI job:
> - `salt-alpine-post-aluminum-not-latest`
> - `salt-alpine-post-magnesium-not-latest`
> - `salt-alpine-pre-magnesium-not-latest`

You can now submit the MR for review.

## After MR is merged: Edit DockerHub README

Once the MR is merged, you need to edit the `Overview` page on
[dockerhub: saltstack/salt](https://hub.docker.com/r/saltstack/salt) with the
`README.md` updates that have been applied to `saltdocker`.

This will require membership in the https://hub.docker.com/u/saltstack DockerHub
Community Organization.

Once the page is updated: missions complete! DockerHub has been updated with
the latest and greatest.
